var fs = require('fs');
var solc = require('solc');
var b = fs.readFileSync('contract.txt',"utf8");
var Web3 = require('web3');
var web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider('http://localhost:1111'));
web3.personal.unlockAccount(web3.eth.accounts[0],"123",30000)

var input = {
   'contract': b
};

var output = solc.compile({sources: input});
for (var contractName in output.contracts){ 
	console.log(contractName);
	var myContract = web3.eth.contract(JSON.parse(output.contracts[contractName].interface)); 
	var bytecode = '0x' + output.contracts[contractName].bytecode;
	var txDeploy = {from:web3.eth.accounts[0], data: bytecode, gas: 1000000}; 
	contractDeploy(txDeploy,myContract);
}
function contractDeploy(txDeploy,myContract){
	var greeter = myContract.new(txDeploy, function(e, contract){
		if(!e) {
			if(!contract.address) {
				console.log("Contract transaction send: TransactionHash: " + contract.transactionHash + " waiting to be mined...");
			} else{
				console.log("Contract mined! Address: " + contract.address);
			}
		}
	})
}
