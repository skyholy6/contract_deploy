var Web3 = require('web3');
var web3 = new Web3();
var fs = require('fs');
var solc = require('solc');
web3.setProvider(new web3.providers.HttpProvider('http://localhost:1111')); 
var b = fs.readFileSync('contract.txt',"utf8");
var contractAddress = "0xfd75d64f09ee90f0eaf55a124fd1727266345123";
var sendAddress = web3.eth.accounts[0];
var dataName = "hear";
var dataContent = "got";
var time = 100000;
var contractName = "contract:data";
var input = {
   'contract': b
};
var output = solc.compile({sources: input});
var v = web3.eth.contract(JSON.parse(output.contracts[contractName].interface)).at(contractAddress);
var res = v.setData.call(sendAddress,dataName,dataContent,time);
console.log('return: ' + res);
