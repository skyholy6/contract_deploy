var Web3 = require('web3');
var web3 = new Web3();
var fs = require('fs');
var solc = require('solc');
web3.setProvider(new web3.providers.HttpProvider('http://localhost:1111')); 
var contractCode = fs.readFileSync('contract.txt',"utf8");
var contractAddress = "0xfd75d64f09ee90f0eaf55a124fd1727266345123";
var sendAddress = web3.eth.accounts[0];
var dataName = "hear";
var dataContent = "got";
var time = 100000;
var contractName = "contract:data";
var input = {
   'contract': contractCode
};
var output = solc.compile({sources: input});

var v = web3.eth.contract(JSON.parse(output.contracts[contractName].interface)).at(contractAddress);
web3.personal.unlockAccount(web3.eth.accounts[0],"123",30000);
v.setData.sendTransaction(web3.eth.accounts[0],"hear","got",100000,{from:web3.eth.accounts[0]},function (err, result) {
	if (err) {
		console.error(err);
		return;
	}
	var txhash = result;
	var filter = web3.eth.filter('latest');
	console.log("TransactionHash: "+result+" waiting to be mined...");
	filter.watch(function(error, result) {
		var receipt = web3.eth.getTransactionReceipt(txhash);
		if (receipt && receipt.transactionHash == txhash) {
			var res = v.setData.call(sendAddress,dataName,dataContent,time);
			console.log("Contract mined! Address: " + result);
			console.log('return: ' + res);
			filter.stopWatching();
		}
	});
});

